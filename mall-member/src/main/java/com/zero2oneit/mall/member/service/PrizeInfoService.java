package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.query.member.PrizeInfoQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
public interface PrizeInfoService extends IService<PrizeInfo> {

    BoostrapDataGrid pageList(PrizeInfoQueryObject qo);


}

