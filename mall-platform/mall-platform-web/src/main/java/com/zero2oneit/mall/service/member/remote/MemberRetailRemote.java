package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.MemberRetailFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/12 19:25
 */
@RestController
@RequestMapping("/remote/retail")
public class MemberRetailRemote {

    @Autowired
    private MemberRetailFeign retailFeign;

    /**
     * 查询分销列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody MemberRetailQueryObject qo) {
        return retailFeign.list(qo);
    }

    /**
     * 添加或编辑分销信息
     * @param retail
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(MemberRetail retail){
        return retailFeign.addOrEdit(retail);
    }

}
