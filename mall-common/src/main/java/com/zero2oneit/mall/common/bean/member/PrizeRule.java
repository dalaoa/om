package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@Data
@TableName("prize_rule")
public class PrizeRule implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id自增
     */
    @TableId
    private Long id;
    /**
     * 每次抽奖耗费多少金币
     */
    private Integer ruleFee;
    /**
     * 每个人抽奖次数：默认0-不限次数 其他按照次数限制
     */
    private Integer ruleCount;
    /**
     * 中奖的基数
     */
    private Integer ruleBase;

}
